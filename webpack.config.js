var path = require('path');
var webpack = require('webpack');
var argv = require('yargs').argv;

var brand = argv.brand || "xfactor";
var rootNode = argv.rootNode || "mocks";
var contentRoot = argv.contentRoot || "";
var debug = argv.debug ? true : false;

console.log("Using brand: " + brand);
console.log("");

console.log("Using host: " + rootNode);
console.log("");

console.log("Using content root: " + contentRoot);
console.log("");

module.exports = {
    entry: [__dirname + '/src/js/main', 'babel-polyfill'],
    output: {
        path: __dirname,
        filename: 'www/js/bundle.js',
        publicPath: "/www/"
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new webpack.DefinePlugin({
            ENV: {
                rootNode: "'" + rootNode + "'",
                contentRoot: "'" + contentRoot + "'",
                debug: debug
            }
        })
    ],
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules|bower_components/,
                query: {
                    presets: ['es2015', 'stage-0']
                }
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!autoprefixer-loader?browsers=last 5 version!less-loader"
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file?name=public/fonts/[name].[ext]'
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /js-api|js-util|fastclick|accounting/,
                loader: "script-loader"
            }
        ]
    },
    resolve: {
        extensions: ['', '.js'],
        modulesDirectories: ['node_modules', 'bower_components'],
        alias: {
            app: path.join(__dirname, 'src/js'),
            config: path.join(__dirname, 'brand/' + brand + '/files/config'),
            style: path.join(__dirname, 'brand/' + brand + '/files/style')
        }
    },
    node: {
        global: true
    }
};
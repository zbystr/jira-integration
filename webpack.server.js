var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
    contentBase: "www",
    headers: { "Access-Control-Allow-Origin": "*" },
    inline: true,
    https: true,
    progress: true,
    historyApiFallback: {
        index: '/index.htm'
    },
    stats: {
        colors: true
    }
}).listen(3000, '0.0.0.0', 'localhost',
    function (err) {
        if (err) {
            console.log(err);
        }
        console.log('Listening at localhost:3000');
    }
);
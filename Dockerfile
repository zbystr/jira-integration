FROM node:5.0.0-slim
EXPOSE 8080
COPY . /web
WORKDIR /web
ENTRYPOINT ["npm", "run", "start-docker"]